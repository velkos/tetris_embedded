#ifndef _GEOM_
#define _GEOM_
#include <math.h>

const double PI = 3.141592653589793;

char isNullPtr(void* ptr) {
	return (char)(ptr == (void*)0);
}

char valueInRange(int value, int min, int max) {
	return (value >= min) && (value <= max);
}

double ToDeg(double rad) {
	return 180/PI * rad;
}

double ToRad(double deg) {
	return PI/180 * deg;
}


typedef struct {
	int x;
	int y;
} Point_t;

void InitPoint(Point_t* p, int x, int y) {
	if(isNullPtr(p)) return;
	p->x = x;
	p->y = y;
}

void DrawPoint(Point_t p, Color_t color) {
	LCDSetPixel(p.x, p.y, color);
}

void RotatePointAround(Point_t* p, Point_t origin, double degrees) {
	double a = ToRad(degrees),
		s = sin(a),
		c = cos(a),
		xdiff = p->x - origin.x,
		ydiff = p->y - origin.y;

	p->x = c * xdiff - s * ydiff + origin.x;
    p->y = s * xdiff + c * ydiff + origin.y;
} 

void MovePoint(Point_t* p, int byX, int byY) {
	if(isNullPtr(p)) return;
	p->x += byX;
	p->y += byY;
}

typedef struct {
	Point_t points[4];
} Rectangle_t;

void InitRectangle(Rectangle_t* r, Point_t p, int w, int h) {
	if(isNullPtr(r)) return;
	r->points[0].x = p.x;
	r->points[0].y = p.y;

	r->points[1].x = p.x + w;
	r->points[1].y = p.y;

	r->points[2].x = p.x + w;
	r->points[2].y = p.y + h;

	r->points[3].x = p.x;
	r->points[3].y = p.y + h;
}

void MoveRectangle(Rectangle_t* r, int byX, int byY) {
	int i = 0;
	if(isNullPtr(r)) return;
	for(; i < 4; ++i) {
		r->points[i].x += byX;
		r->points[i].y += byY;
	}
}

void GetRectangleData(Rectangle_t r, int* x, int* y, int* width, int* height) {
	int highestX = r.points[3].x, 
		highestY = r.points[3].y, 
		lowestX = r.points[0].x, 
		lowestY = r.points[0].y, 
		i = 0;
	if(isNullPtr(x) || isNullPtr(y) || isNullPtr(width) || isNullPtr(height)) return;
	for(; i < 4; ++i) {
		if(r.points[i].x > highestX) {
			highestX = r.points[i].x;
		}
		if(r.points[i].x < lowestX) {
			lowestX = r.points[i].x;
		}

		if(r.points[i].y > highestY) {
			highestY = r.points[i].y;
		}
		if(r.points[i].y < lowestY) {
			lowestY = r.points[i].y;
		}
	}

	/*Fill in the data into the variables passed*/
	*x = lowestX;
	*y = lowestY;
	*width = (highestX - lowestX);
	*height = (highestY - lowestY);
}

void FillRectangle(Rectangle_t r, Color_t color) {
	int x, y, width, height;
	GetRectangleData(r, &x, &y, &width, &height);
	LCDSetRect(x, y, x + width, y + height, 1, color);
}

void StrokeRectangle(Rectangle_t r, Color_t color) {
	int i = 0;
	LCDSetLine(r.points[0].x, r.points[0].y, r.points[3].x, r.points[3].y, color);
	for(; i < 3; ++i) {
		LCDSetLine(r.points[i].x, r.points[i].y, r.points[i + 1].x, r.points[i + 1].y, color);
	}
}

char IntersectingRectangles(Rectangle_t r1, Rectangle_t r2) {
	int r1_x, r1_y, r1_w, r1_h,
		r2_x, r2_y, r2_w, r2_h;
	char xOverlap, yOverlap;
	GetRectangleData(r1, &r1_x, &r1_y, &r1_w, &r1_h);
	GetRectangleData(r2, &r2_x, &r2_y, &r2_w, &r2_h);
	xOverlap = valueInRange(r1_x, r2_x, r2_x + r2_w) ||
    	valueInRange(r2_x, r1_x, r1_x + r1_w);
    yOverlap = valueInRange(r1_y, r2_y, r2_y + r2_h) ||
    	valueInRange(r2_y, r1_y, r1_y + r1_h);
	return xOverlap && yOverlap;
}

void RotateRectangleAround(Rectangle_t* r, Point_t p, double degrees) {
	int i = 0;
	if(isNullPtr(r)) return;
	for(; i < 4; ++i) {
		RotatePointAround(&r->points[i], p, degrees);
	}
}

#endif