#ifndef _TETRIS_
#define _TETRIS_

#include "tetromino.h"
#include "input.h"
#include <stdlib.h>

const char TETROMINO_TYPES[] = {'i', 'l', 'z', 's', 'j', 'o', 't'};

const int TETRIS_FALL_SPEED = 5,
		  TETRIS_STEER_SPEED = 5,
		  DISPLAY_WIDTH = 130,
		  DISPLAY_HEIGHT = 130,
		  TETROMINO_TYPES_LENGTH = sizeof(TETROMINO_TYPES) / sizeof(char);

const Color_t TETROMINO_COLORS[] = {
	LCD_COLOR_CYAN,
	LCD_COLOR_ORANGE,
	LCD_COLOR_RED,
	LCD_COLOR_GREEN,
	LCD_COLOR_BLUE,
	LCD_COLOR_YELLOW,
	LCD_COLOR_PINK
};

typedef struct {
	Tetromino_t current;
	Tetromino_t board[10];
	char blocksOnBoard;
	char fallSpeed;
	char steerSpeed;
	char widthInBlocks;
	char heightInBlocks;
	int width;
	int height;
} Tetris_t;

void RandomizeTetromino(Tetromino_t* t, Point_t p) {
	if(isNullPtr(t)) return;
	InitTetromino(t, p, TETROMINO_TYPES[rand() % TETROMINO_TYPES_LENGTH]);
}

void InitTetris(Tetris_t* t, int width, int height) {
	if(isNullPtr(t)) return;
	t->blocksOnBoard = 0;
	t->fallSpeed = TETRIS_FALL_SPEED;
	t->steerSpeed = TETRIS_STEER_SPEED;
	t->width = width;
	t->height = height;
	t->widthInBlocks = width / TETROMINO_BLOCK_SIZE;
	t->heightInBlocks = height / TETROMINO_BLOCK_SIZE;
}

void DrawTetronimo(Tetromino_t* t) {
	int i = 0;
	if(isNullPtr(t)) return;
	for(; i < TETROMINO_TYPES_LENGTH; ++i) {
		if(t->type == TETROMINO_TYPES[i]) {
			FillTetromino(*t, TETROMINO_COLORS[i]);
		}
	}
}

void DrawTetris(Tetris_t* t) {
	int i = 0;
	if(isNullPtr(t)) return;
	for(; i < t->blocksOnBoard; ++i) {
		DrawTetronimo(&t->board[i]);
	}
}

void UpdateTetris(Tetris_t* t) {
	int i = 0;
	char intersecting = 0, 
		 moved = 0;
	/*these are the moved possible values:
		1 - left
		2 - down
		3 - right
	*/
	LCD_ClearScreen(LCD_COLOR_WHITE);
	if(isNullPtr(t)) return;
	
	/*Move the active block according to input*/
	if(IsJoystickLeft()) {
		MoveTetromino(&t->current, -t->steerSpeed, 0);
		moved = 1;
	}
	else if(IsJoystickDown()) {
		MoveTetromino(&t->current, 0, t->fallSpeed);
		moved = 2;
	}
	else if(IsJoystickRight()) {
		MoveTetromino(&t->current, t->steerSpeed, 0);
		moved = 3;
	}
	MoveTetromino(&t->current, 0, t->fallSpeed);
	
	for(; i < t->blocksOnBoard; ++i) {
		if(IntersectingTetrominoes(t->current, t->board[i])) {
			intersecting = 1;
			break;
		}
	}

	/*If the current block intersects blocks on board reverse the movement and add the block to the board*/
	if(intersecting) {
		switch(moved) {
			case 1: 
				MoveTetromino(&t->current, t->steerSpeed, 0);
				break;
			case 2:
				MoveTetromino(&t->current, 0, -t->fallSpeed);
				break;
			case 3:
				MoveTetromino(&t->current, -t->steerSpeed, 0);
				break;
		}
		t->board[t->blocksOnBoard] = t->current;
		t->blocksOnBoard++;

		//RandomizeTetromino(&t->current, THEPOINT);
	}
	DrawTetris(t);
}

#endif