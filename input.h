#ifndef _INPUT_
#define _INPUT_

#include "ioat91sam7x256.h"

#define LEFT_BUTTON_MASK 0x01000000
#define RIGHT_BUTTON_MASK 0x02000000
#define JOYSTICK_LEFT_MASK 0x00000080
#define JOYSTICK_DOWN_MASK 0x00000100
#define JOYSTICK_UP_MASK 0x00000200
#define JOYSTICK_RIGHT_MASK 0x00004000
#define JOYSTICK_PUSH_MASK 0x00008000

AT91PS_PIO    p_pPioA, p_pPioB;
AT91PS_PMC    p_pPMC;
AT91PS_SYS    p_pSys;

void InitInput() {
	p_pPioA  = AT91C_BASE_PIOA;
	p_pPioB  = AT91C_BASE_PIOB;
	p_pPMC   = AT91C_BASE_PMC;
	p_pSys   = AT91C_BASE_SYS;
	//enable the clock of the PIO
	p_pPMC->PMC_PCER = 1 << AT91C_ID_PIOA;
	//enable the clock of the PIO
	p_pPMC->PMC_PCER = 1 << AT91C_ID_PIOB;

	// GPIO init
	p_pPioA->PIO_ODR    = 0xffffffff;   // All as input
	p_pPioB->PIO_ODR    = 0xffffffff;   // All as input
	p_pSys->PIOA_PPUDR  = 0xffffffff;   // Disable Pull-up resistor
	p_pSys->PIOB_PPUDR  = 0xffffffff;   // Disable Pull-up resistor

	// BUTTON SW1
	p_pPioB->PIO_ODR |= LEFT_BUTTON_MASK; //Configure in Input
	p_pPioB->PIO_PER |= LEFT_BUTTON_MASK; //Enable PB24

	// BUTTON SW2
	p_pPioB->PIO_ODR |= RIGHT_BUTTON_MASK; //Configure in Input
	p_pPioB->PIO_PER |= RIGHT_BUTTON_MASK; //Enable PB25

	// BUTTON SW2
	p_pPioA->PIO_ODR |= JOYSTICK_UP_MASK; //Configure in Input
	p_pPioA->PIO_PER |= JOYSTICK_UP_MASK; //Enable PB25

	p_pPioA->PIO_ODR |= JOYSTICK_LEFT_MASK; //Configure in Input
	p_pPioA->PIO_PER |= JOYSTICK_LEFT_MASK; //Enable PB25

	p_pPioA->PIO_ODR |= JOYSTICK_DOWN_MASK; //Configure in Input
	p_pPioA->PIO_PER |= JOYSTICK_DOWN_MASK; //Enable PB25

	p_pPioA->PIO_ODR |= JOYSTICK_RIGHT_MASK; //Configure in Input
	p_pPioA->PIO_PER |= JOYSTICK_RIGHT_MASK; //Enable PB25

	p_pPioA->PIO_ODR |= JOYSTICK_PUSH_MASK; //Configure in Input
	p_pPioA->PIO_PER |= JOYSTICK_PUSH_MASK; //Enable PB25
}

char IsLeftButtonDown() {
	return !((p_pPioB->PIO_PDSR) & LEFT_BUTTON_MASK);
}

char IsRightButtonDown() {
	return !((p_pPioB->PIO_PDSR) & RIGHT_BUTTON_MASK);
}

char IsJoystickUp() {
 	return !((p_pPioA->PIO_PDSR) & JOYSTICK_UP_MASK);
}

char IsJoystickLeft() {
	return !((p_pPioA->PIO_PDSR) & JOYSTICK_LEFT_MASK);
}

char IsJoystickDown() {
	return !((p_pPioA->PIO_PDSR) & JOYSTICK_DOWN_MASK);
}

char IsJoystickRight() {
	return !((p_pPioA->PIO_PDSR) & JOYSTICK_RIGHT_MASK);
}

char IsJoystickPushed() {
	return !((p_pPioA->PIO_PDSR) & JOYSTICK_PUSH_MASK);
}

#endif 