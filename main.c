#include <basedef.h>
#include <Board.h>
#include <stdio.h>

#include <fixedsys.h>
#include <lcd_GE8.h>
#include "tetris.h"

void Delay (unsigned long a) {while (--a!=0);}

int main()
{
	Tetris_t tetris;

	/* any other variables you need ... */
	/* Initialize the Atmel AT91SAM7X256 (watchdog, PLL clock, default interrupts, etc.) */
	AT91F_LowLevel_Init();
	InitInput();
	/* enable interrupts */
	AT91F_Finalize_Init();
	InitLCD();
	LCD_ClearScreen(LCD_COLOR_WHITE);

	InitTetris(&tetris, 130, 130);
	
	/* your main loop ... */
	while ( true )
	{
		//UpdateTetris(&tetris);
	}
	/* The answer of every question in the universe */
	return 42;
}
