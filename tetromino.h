#ifndef _TETROMINO_
#define _TETROMINO_
#include "geom.h"

const int TETROMINO_BLOCK_SIZE = 10;

typedef struct {
	Rectangle_t blocks[4];
	Point_t center;
	unsigned int angle;
	char type;
} Tetromino_t;

void InitTetromino(Tetromino_t* t, Point_t center, char type) {
	Point_t p;
	if(isNullPtr(t)) return;
	switch(type) {
		case 'l':
			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -1.5);
			InitRectangle(&t->blocks[0], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[1], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * 0.5);
			InitRectangle(&t->blocks[2], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * 0.5, center.y + TETROMINO_BLOCK_SIZE * 0.5);
			InitRectangle(&t->blocks[3], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&t->center, center.x, center.y);
			t->type = type;
			break;
		case 'o':
			InitPoint(&p, center.x - TETROMINO_BLOCK_SIZE, center.y - TETROMINO_BLOCK_SIZE);
			InitRectangle(&t->blocks[0], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x - TETROMINO_BLOCK_SIZE, center.y);
			InitRectangle(&t->blocks[1], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x, center.y);
			InitRectangle(&t->blocks[2], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x, center.y - TETROMINO_BLOCK_SIZE);
			InitRectangle(&t->blocks[3], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&t->center, center.x, center.y);
			t->type = type;
			break;
		case 'i':
			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -2, center.y + TETROMINO_BLOCK_SIZE * -1);
			InitRectangle(&t->blocks[0], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -1, center.y + TETROMINO_BLOCK_SIZE * -1);
			InitRectangle(&t->blocks[1], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x, center.y + TETROMINO_BLOCK_SIZE * -1);
			InitRectangle(&t->blocks[2], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE, center.y + TETROMINO_BLOCK_SIZE * -1);
			InitRectangle(&t->blocks[3], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&t->center, center.x, center.y);
			t->type = type;
			break;
		case 's':
			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -1.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[0], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[1], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -1.5);
			InitRectangle(&t->blocks[2], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * 0.5, center.y + TETROMINO_BLOCK_SIZE * -1.5);
			InitRectangle(&t->blocks[3], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&t->center, center.x, center.y);
			t->type = type;
			break;
		case 't':
			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -1.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[0], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[1], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -1.5);
			InitRectangle(&t->blocks[2], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * 0.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[3], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&t->center, center.x, center.y);
			t->type = type;
			break;

		case 'j':
			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -1.5);
			InitRectangle(&t->blocks[0], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[1], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * 0.5);
			InitRectangle(&t->blocks[2], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -1.5, center.y + TETROMINO_BLOCK_SIZE * 0.5);
			InitRectangle(&t->blocks[3], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&t->center, center.x, center.y);
			t->type = type;
			break;

		case 'z':
			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -1.5, center.y + TETROMINO_BLOCK_SIZE * -1.5);
			InitRectangle(&t->blocks[0], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -1.5);
			InitRectangle(&t->blocks[1], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * -0.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[2], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&p, center.x + TETROMINO_BLOCK_SIZE * 0.5, center.y + TETROMINO_BLOCK_SIZE * -0.5);
			InitRectangle(&t->blocks[3], p, TETROMINO_BLOCK_SIZE, TETROMINO_BLOCK_SIZE);

			InitPoint(&t->center, center.x, center.y);
			t->type = type;
			break;
	}
}

void RotateTetromino(Tetromino_t* t, Point_t p, int degrees) {
	int i = 0;
	/*If the tetromino is not ok or we're trying to rotate to angle which cant be divided evenly by 90*/
	if(isNullPtr(t) || degrees % 90 != 0) return;
	for(; i < 4; ++i) {
		RotateRectangleAround(&t->blocks[i], p, (int)degrees);
	}
}

void StrokeTetromino(Tetromino_t t, Color_t color) {
	int i = 0;
	for(; i < 4; ++i) {
		StrokeRectangle(t.blocks[i], color);
	}
}

void FillTetromino(Tetromino_t t, Color_t color) {
	int i = 0;
	for(; i < 4; ++i) {
		FillRectangle(t.blocks[i], color);
	}
}

char IntersectingTetrominoes(Tetromino_t t1, Tetromino_t t2) {
	int i, i2;
	for(i = 0; i < 4; ++i) {
		for(i2 = 0; i2 < 4; ++i2) {
			if(IntersectingRectangles(t1.blocks[i], t2.blocks[i2])) return 1;
		}
	}
	return 0;
}

void MoveTetromino(Tetromino_t* t, int byX, int byY) {
	int i;
	if(isNullPtr(t)) return;
	for(i = 0; i < 4; ++i) {
		MoveRectangle(&t->blocks[i], byX, byY);
	}
	MovePoint(&t->center, byX, byY);
}

#endif